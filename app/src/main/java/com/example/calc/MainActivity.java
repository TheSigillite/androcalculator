package com.example.calc;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.stream.LongStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //zapamiętwyanie stanu zmiennych przy zmianie orientacji ekranu
        //zmienne są zapisywane pod kluczami
        super.onSaveInstanceState(outState);
        TextView textView = findViewById(R.id.CurrentEq);
        outState.putDouble("VAR1",eqnum1);
        outState.putDouble("VAR2",eqnum2);
        outState.putBoolean("DIV",division);
        outState.putBoolean("MUL",multi);
        outState.putBoolean("ADD",addition);
        outState.putBoolean("SUB",subtraction);
        outState.putBoolean("MOD",modul);
        try {
            outState.putString("CURRENT",textView.getText().toString());
        } catch (Exception e){
            outState.putString("CURRENT","");
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        //odczyt stanu zmiennych przy zmianie orientacji ekranu
        //zmienne są odczytywane z pod kluczy
        super.onRestoreInstanceState(savedInstanceState);
        TextView textView = findViewById(R.id.CurrentEq);
        eqnum1 = savedInstanceState.getDouble("VAR1");
        eqnum2 = savedInstanceState.getDouble("VAR2");
        division = savedInstanceState.getBoolean("DIV");
        multi = savedInstanceState.getBoolean("MUL");
        addition = savedInstanceState.getBoolean("ADD");
        subtraction = savedInstanceState.getBoolean("SUB");
        modul = savedInstanceState.getBoolean("MOD");
        String textfieldcontent = savedInstanceState.getString("CURRENT");
        textView.setText(textfieldcontent);
    }
    //zmienne kalkulatora
    boolean division = false;
    boolean multi = false;
    boolean addition = false;
    boolean subtraction = false;
    boolean modul = false;
    double eqnum1 = 0.0;
    double eqnum2 = 0.0;

    public void Equate(View view) {
        // /\
        //sprawdzenie który przycisk był wciśnięty
        //========================================
        //twożenie toastów do exceptions i działania
        // \/

        TextView textView = findViewById(R.id.CurrentEq);
        Context context = getApplicationContext();
        Toast nulltoast = Toast.makeText(context,"No value in the input field",Toast.LENGTH_SHORT);
        nulltoast.setGravity(Gravity.TOP,0,0);
        Toast numbertoast = Toast.makeText(context,"Multiple decimal dots in number",Toast.LENGTH_SHORT);
        numbertoast.setGravity(Gravity.TOP,0,0);
        Toast exctoast = Toast.makeText(context,"An unknown exception has occured",Toast.LENGTH_SHORT);
        exctoast.setGravity(Gravity.TOP,0,0);
        switch(view.getId()){
            case R.id.ACButton: // AC
                eqnum1 = 0.0;
                eqnum2 = 0.0;
                textView.setText("");
                break;
            case R.id.DivButton:// /
                try{
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                    eqnum1 = Double.parseDouble(textView.getText().toString());
                    textView.setText("");
                    if(multi==false && addition==false && subtraction==false && modul==false) {
                        division = true;
                   }}catch (NullPointerException e){
                    nulltoast.show(); }
                    catch (NumberFormatException f){
                    numbertoast.show();
                    }
                    catch (Exception n){
                    exctoast.show(); }
                break;
            case R.id.EqualsButton: //=
                try {
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                    if(division==true){
                        eqnum2 = Double.parseDouble(textView.getText().toString());
                        eqnum1 = eqnum1/eqnum2;
                        textView.setText(eqnum1+"");
                        division = false;
                        eqnum2 = 0.0;
                    }else if(multi==true) {
                        eqnum2 = Double.parseDouble(textView.getText().toString());
                        eqnum1 = eqnum1 * eqnum2;
                        textView.setText(eqnum1 + "");
                        multi = false;
                        eqnum2 = 0.0;
                    }else if(subtraction==true){
                        eqnum2 = Double.parseDouble(textView.getText().toString());
                        eqnum1 = eqnum1 - eqnum2;
                        textView.setText(eqnum1 + "");
                        subtraction = false;
                        eqnum2 = 0.0;
                    }else if(addition==true){
                        eqnum2 = Double.parseDouble(textView.getText().toString());
                        eqnum1 = eqnum1 + eqnum2;
                        textView.setText(eqnum1 + "");
                        addition = false;
                        eqnum2 = 0.0;
                    }else if(modul==true){
                        eqnum2 = Double.parseDouble(textView.getText().toString());
                        eqnum1 = eqnum1 % eqnum2;
                        textView.setText(eqnum1 + "");
                        modul = false;
                        eqnum2 = 0.0;
                    }
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.LogButton: //log(10)
                try{
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                    eqnum1 = Double.parseDouble(textView.getText().toString());
                    eqnum1 = Math.log10(eqnum1);
                    textView.setText(eqnum1+"");
                    }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.MultiplyButton: //*
                try {
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                        eqnum1 = Double.parseDouble(textView.getText().toString());
                        textView.setText("");
                    if (division == false && addition == false && subtraction == false && modul == false) {
                        multi = true;
                    }
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.MarkButton: //+/-
                try{
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                        String analyze = textView.getText().toString();
                        if(analyze.substring(0,1).equals("-")){
                            textView.setText(analyze.substring(1));
                        }else{
                            textView.setText("-"+analyze);
                        }
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.SubtarctButton: // -
                try{
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                        eqnum1 = Double.parseDouble(textView.getText().toString());
                        textView.setText("");
                    if (division == false && addition == false && multi == false && modul == false) {
                        subtraction = true;
                    }
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.SumButton: // +
                try{
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                        eqnum1 = Double.parseDouble(textView.getText().toString());
                        textView.setText("");
                    if (division == false && subtraction == false && multi == false && modul == false) {
                        addition = true;
                    }
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.ModButton: // %
                try{
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                        eqnum1 = Double.parseDouble(textView.getText().toString());
                        textView.setText("");
                    if (division == false && subtraction == false && multi == false && addition == false) {
                        modul = true;
                    }
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.PowButton: // ! //overflows after 25!
                try{
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                    long eqnumlong = Long.parseLong(textView.getText().toString());
                    long factorial = 1;
                    if(eqnumlong>2){
                        for(int i = 2;i<=eqnumlong;i++)
                            factorial = factorial*i;
                    }
                    textView.setText(factorial+"");
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){
                    numbertoast.setText("Factorial operation cannot be performed on decimal numbers");
                    numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.SqrtButton: //sqrt
                try {
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                    eqnum1 = Double.parseDouble(textView.getText().toString());
                    eqnum1 = Math.sqrt(eqnum1);
                    textView.setText(eqnum1 + "");
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.SquareButton: //^2
                try {
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                    eqnum1 = Double.parseDouble(textView.getText().toString());
                    eqnum1 = eqnum1*eqnum1;
                    textView.setText(eqnum1 + "");
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            case R.id.TriangleButton://^3
                try {
                    if(textView.getText().toString().isEmpty()) throw new NullPointerException();
                    eqnum1 = Double.parseDouble(textView.getText().toString());
                    eqnum1 = eqnum1*eqnum1*eqnum1;
                    textView.setText(eqnum1 + "");
                }catch (NullPointerException e){ nulltoast.show(); }
                catch (NumberFormatException f){ numbertoast.show(); }
                catch (Exception n){ exctoast.show(); }
                break;
            default:
                Button button = findViewById(view.getId());
                textView.append(button.getText());
        }
    }
}